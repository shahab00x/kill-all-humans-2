// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFPSProject3.h"
#include "SideBuilding.h"
#include <vector>




ASideBuilding::ASideBuilding(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer){

	mainMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("mainMesh"));
	const ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObj(meshFileReference);
	mainMesh->SetStaticMesh(MeshObj.Object);
	RootComponent = mainMesh;

/*	static ConstructorHelpers::FObjectFinder<UMaterial> MatFinder(TEXT("MaterialInstanceConstant'/Game/Materials/fadeIn.fadeIn'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> textureFinder(textureFileReference);

	if (MatFinder.Succeeded() && textureFinder.Succeeded())
	{
		UMaterial* Material = MatFinder.Object;
		UTexture2D * myTexture = textureFinder.Object;
		MaterialInstance = UMaterialInstanceDynamic::Create(Material, this);

		MaterialInstance->SetScalarParameterValue(FName("opacityValue"), 0);
//		MaterialInstance->SetTextureParameterValue(FName("myTexture"), myTexture);
		mainMesh->SetMaterial(0, MaterialInstance);
	}*/

	

}

void ASideBuilding::BeginPlay(){
	UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Materials/fadeIn.fadeIn'")));

	UTexture2D * myTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, textureFileReference));

	MaterialInstance = UMaterialInstanceDynamic::Create(Material, this);

	MaterialInstance->SetTextureParameterValue(FName("myTexture"), myTexture);
	MaterialInstance->SetScalarParameterValue(FName("opacityValue"), 0);
	mainMesh->SetMaterial(0, MaterialInstance);
}

/*void ASideBuilding::PostInitializeComponents(){
	UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Materials/fadeIn.fadeIn'")));
	
	UTexture2D * myTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, textureFileReference));

	MaterialInstance = UMaterialInstanceDynamic::Create(Material, this);

	MaterialInstance->SetTextureParameterValue(FName("myTexture"), myTexture);
	MaterialInstance->SetScalarParameterValue(FName("opacityValue"), 0);
	
	mainMesh->SetMaterial(0, MaterialInstance);
}
*/
const FRotator ASideBuilding::rotation = FRotator(0, 90, 0);
const std::vector<FRotator> ASideBuilding::rotations{ FRotator(0, 90, 0), FRotator(0, 90, 0) };
const std::vector<FVector> ASideBuilding::translations{ FVector(0,0,0), FVector(1200,0,0) };