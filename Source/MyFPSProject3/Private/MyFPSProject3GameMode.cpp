// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "MyFPSProject3.h"
#include "MyFPSProject3GameMode.h"
#include "MyFPSProject3HUD.h"
#include "MyFPSProject3Character.h"

AMyFPSProject3GameMode::AMyFPSProject3GameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/MyCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyFPSProject3HUD::StaticClass();
}
