// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFPSProject3.h"
#include "Human.h"
#include <Engine.h>
#include <UnrealString.h>
#include "MyFPSProject3Projectile.h"

AHuman::AHuman(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	{
		HP = 100.0f;
		CharacterMovement->bRunPhysicsWithNoController = true;
	}


void AHuman::FindPlayer(float DeltaSeconds){
	// Get the pointer to the first player controller
	APlayerController* player = this->GetWorld()->GetFirstPlayerController();
	
	// Find the location of the current player
	FVector position = player->AcknowledgedPawn->GetActorLocation();
	FVector direction = position - this->GetActorLocation();

	float dist = position.Dist(this->GetActorLocation(), position);
	
	FString distStr;
	distStr = distStr.SanitizeFloat(dist);
	//logToScreen(distStr, false);
	
	this->GetCharacterMovement()->MovementMode = MOVE_Walking;
	this->GetCharacterMovement()->MaxWalkSpeed = 50;
	this->FaceRotation(direction.Rotation());

	if (dist > 200.0f){
		this->GetCharacterMovement()->MoveSmooth(direction, DeltaSeconds);
	}
}

void AHuman::BeginPlay() {
	logToScreen("BeginPlayer()");
};

void AHuman::ReceiveTick(float DeltaSeconds){
	FindPlayer(DeltaSeconds);
}


void AHuman::ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, 
	class UPrimitiveComponent* OtherComp, bool bSelfMoved, 
	FVector HitLocation, FVector HitNormal, FVector NormalImpulse, 
	const FHitResult& Hit){

	logToScreen("Hit", true);
	Super::ReceiveHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	
	logToScreen("ReceiveHit", true);
	bool isProjectile = false;
	if (dynamic_cast<AMyFPSProject3Projectile*>(Other))
	{
		isProjectile = true;
		logToScreen("true");
	}
	
	if (Other && (Other != this) && OtherComp && isProjectile)
		HP -= 20;

	if (HP <= 0)
		Destroy();
	logToScreen("Collision", false);
	logToScreen(FString::FromInt(HP), true);
}

void AHuman::updateMaterial(){

}

const FRotator AHuman::rotation = FRotator(0, 90, 0);