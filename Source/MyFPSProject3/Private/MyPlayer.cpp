// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFPSProject3.h"
#include "MyPlayer.h"
#include <Engine.h>
#include <UnrealString.h>
#include "FloorActor.h"
#include <Vector.h>
#include "SideBuilding.h"
#include "Human.h"
#include <time.h>

AMyPlayer::AMyPlayer(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
//	AFloorActor::meshFileReference = TEXT("StaticMesh'/Game/Data/Floor_400x400.Floor_400x400'");
	
	currentFloorsize = FVector(1400, 1000, 10);
	tileSideSize = 10;
	std::vector<std::vector<AActor*>> meshArray{ };

	static ConstructorHelpers::FObjectFinder<UBlueprint> ItemBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Human2.BP_Human2'"));
	if (ItemBlueprint.Object)
		humanBlueprint = (UClass*)ItemBlueprint.Object->GeneratedClass;

}

void AMyPlayer::BeginPlay(){
	logToScreen("BeginPlay()");
	srand(time(0));

	if (meshArray.size() == 0)
		for (int i = 0; i < 4; i++)
			meshArray.push_back(std::vector < AActor* > {});
	

	FVector start(0, 0, 0);
	start. Y += -(tileSideSize/2)*currentFloorsize.Y;
	for (int i = 0; i < tileSideSize; i++){
		GenerateMesh(start, "y+");
		//start.Y += currentFloorsize.Y;
	}
	currentCell = FVector();
}

void AMyPlayer::ReceiveTick(float DeltaSeconds){
	
	// Get the pointer to the first player controller
	APlayerController* player = this->GetWorld()->GetFirstPlayerController();

	// Find the location of the current player
	playerPosition = player->AcknowledgedPawn->GetActorLocation();
	this->SetActorLocation(playerPosition);

	handleFloorPlacement();
}
void AMyPlayer::handleFloorPlacement(){
	int n = playerIsOnWhichTile();
	FString dir = isOnMiddleTile();
	if ( dir.Compare("true") != 0) // if the player is not on the middle tile
	{
		logToScreen(dir);
		int n = playerIsOnWhichTile();
		GenerateMesh(meshArray[0][n]->GetActorLocation(), dir);
	}
	
}

FString AMyPlayer::isOnMiddleTile(){
	if (playerPosition.X > currentCell.X && playerPosition.X < currentCell.X + currentFloorsize.X &&
		playerPosition.Y > currentCell.Y && playerPosition.Y < currentCell.Y + currentFloorsize.Y)
		return "true";
	else{
		int n = playerIsOnWhichTile();
		if (n > -1){
			int x = meshArray[0][n]->GetActorLocation().X - currentCell.X;
			int y = meshArray[0][n]->GetActorLocation().Y - currentCell.Y;

			currentCell = meshArray[0][n]->GetActorLocation();

			if (abs(x) > abs(y))
				if (x > 0)
					return "x+";
				else
					return "x-";
			else
				if (y > 0)
					return "y+";
				else
					return "y-";
		}
	}
	return "true";
}
bool AMyPlayer::playerIsOnTile(int tileNumber){
	FVector tilePosition = meshArray[0][tileNumber]->GetActorLocation();
	if (playerPosition.X >= tilePosition.X && playerPosition.X <= tilePosition.X + currentFloorsize.X &&
		playerPosition.Y >= tilePosition.Y && playerPosition.Y <= tilePosition.Y + currentFloorsize.Y)
		return true;
	return false;
}

int AMyPlayer::playerIsOnWhichTile(){
	for (int i = 0; i < meshArray[0].size(); i++)
		if (playerIsOnTile(i))
			return i;
	return -1;
}
AStaticMeshActor* AMyPlayer::GetStaticMesh(FString compName){
	for (TActorIterator<AStaticMeshActor> Itr(GetWorld()); Itr; ++Itr)
	{
		logToScreen(Itr->GetName());
		if (Itr->GetName().StartsWith(compName))
		{
			return *Itr;
		}
	}
	return NULL;
}

int AMyPlayer::tilesDistance(std::vector<AActor*>  meshes, int thisTile, int thatTile){
	float x_dist = meshes[thisTile]->GetActorLocation().X - meshes[thatTile]->GetActorLocation().X; x_dist = abs(x_dist);
	int x = (int)x_dist / currentFloorsize.X;

	float y_dist = meshes[thisTile]->GetActorLocation().Y - meshes[thatTile]->GetActorLocation().Y; y_dist = abs(y_dist);
	int y = (int)y_dist / currentFloorsize.Y;
	if (x > y) return x;
	else return y;
}


void AMyPlayer::GenerateMesh(FVector center, FString dir){
	
	// Create the floor
	DropMesh<AFloorActor>(AFloorActor::StaticClass(), 0, center, dir, tileSideSize, AFloorActor::rotation, FVector(0, 0, 0));
	currentFloorsize = meshArray[0][0]->GetComponentsBoundingBox().GetSize();
	// Create the left wall
	DropMesh<ASideBuilding>(ASideBuilding::StaticClass(), 1, center, dir, tileSideSize, ASideBuilding::rotation, FVector(0, 0, 0));
	// Create the right wall
	DropMesh<ASideBuilding>(ASideBuilding::StaticClass(), 2, center, dir, tileSideSize, ASideBuilding::rotation, FVector(currentFloorsize.X, 0, 0));
	// Create human
	if (rand() % 10 < 3)
		DropMesh<AHuman>(humanBlueprint, 3, center, dir, tileSideSize, AHuman::rotation, FVector(currentFloorsize.X/2, 0, 87));
	
}

template <class T>
void AMyPlayer::DropMesh(UClass* staticClass,
	int meshIndex /* the index to use from the meshArray*/, 
	FVector center /* Where to drop the mesh*/, 
	FString dir, /* The direction that the player is moving : x+, x-, y+, y-, z+, z-*/
	int tileSideLength , 
	FRotator rotation, 
	FVector translation){

	FVector location = FVector(center);
	//location.X += (i - 1) * currentFloorsize.X;
	
	if (meshArray[meshIndex].size() > 0){
		if (dir.Compare("y+") == 0)
			location.Y = currentFloorsize.Y * (meshIndex == 0) + meshArray[0].back()->GetActorLocation().Y;
		if (dir.Compare("y-") == 0)
			location.Y = -currentFloorsize.Y * (meshIndex == 0) + meshArray[0].front()->GetActorLocation().Y;
	}

	FActorSpawnParameters fsp;
	location = location + translation;

	T* fa = this->GetWorld()->SpawnActor<T>(staticClass, location, rotation, fsp);
	//this->GetWorld()->SpawnActor<AHuman>(AHuman::StaticClass(), location + currentFloorsize/2, rotation, fsp);
	fa->SetOwner(this->GetOwner());
	this->GetWorld()->GetTimerManager().SetTimer(fa,&T::updateMaterial, .1f, true);
			
	
	if (dir.Compare("y+") == 0){
		if (meshArray[meshIndex].size() >= tileSideLength){
			if (!meshArray[meshIndex].front()->IsPendingKill())meshArray[meshIndex][0]->Destroy();
			meshArray[meshIndex].erase(meshArray[meshIndex].begin()); // remove from the beginning
		}
		meshArray[meshIndex].push_back(fa); // add to the end
	}
	else if (dir.Compare("y-") == 0){
		if (meshArray[meshIndex].size() >= tileSideLength){
			if (!meshArray[meshIndex].back()->IsPendingKill()) meshArray[meshIndex].back()->Destroy();
			meshArray[meshIndex].pop_back(); // remove from the end
		}
		meshArray[meshIndex].insert(meshArray[meshIndex].begin(), fa); // add to the beginning 
	}
}