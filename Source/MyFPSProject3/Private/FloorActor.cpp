// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFPSProject3.h"
#include "FloorActor.h"
#include <vector>

AFloorActor::AFloorActor(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer){

	mainMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("mainMesh"));
	const ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObj(meshFileReference);
	mainMesh->SetStaticMesh(MeshObj.Object);
	
	

	RootComponent = mainMesh;
	//ConstructorHelpers::FObjectFinder<UTexture2D> textureFinder(textureFileReference);
	//ConstructorHelpers::FObjectFinder<UMaterial> MatFinder(TEXT("Material'/Game/Materials/fadeIn.fadeIn'"));

}

void AFloorActor::updateMaterial(){

	if (MaterialInstance != NULL)
	{
		float v;
		MaterialInstance->GetScalarParameterValue("opacityValue", v);
		if (v < 1.0)
			MaterialInstance->SetScalarParameterValue("opacityValue", v + .1);
	}
}

void AFloorActor::BeginPlay(){
	
	//if (MatFinder.Succeeded() && textureFinder.Succeeded())
	//{
	UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Materials/fadeIn.fadeIn'")));

	UTexture2D * myTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, textureFileReference));

	MaterialInstance = UMaterialInstanceDynamic::Create(Material, this);

	MaterialInstance->SetTextureParameterValue(FName("myTexture"), myTexture);
	MaterialInstance->SetScalarParameterValue(FName("opacityValue"), 0);
	mainMesh->SetMaterial(0, MaterialInstance);
	//}
}

const FRotator AFloorActor::rotation = FRotator(0, 0, 0);
const std::vector<FRotator> AFloorActor::rotations{ FRotator(0, 0, 0)};
const std::vector<FVector> AFloorActor::translations{ FVector(0,0,0)};