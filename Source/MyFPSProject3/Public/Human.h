// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <Engine.h>
#include "GameFramework/Character.h"
#include "Human.generated.h"

/**
 * 
 */
UCLASS()
class MYFPSPROJECT3_API AHuman : public ACharacter
{
	GENERATED_UCLASS_BODY()

	void logToScreen(FString s, bool clearPrevious = false, float duration = 5.0f){
		if (clearPrevious) GEngine->ClearOnScreenDebugMessages();
		GEngine->AddOnScreenDebugMessage(-1, duration, FColor::Blue, s);
	}
	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Entity)
	float HP; // The Health Points for this human

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = SkelMeshComponents)
	UWorld* RoyalSwordL;


	/** Static Mesh Comp, Set In BP Default Properties */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = StaticMeshComponents)
	USkeletalMeshComponent* mainMesh;
	
	UFUNCTION(BlueprintCallable, Category = StaticMeshComponents)
	void FindPlayer(float DeltaSeconds);

	static TSubclassOf<AHuman> myItemBlueprint;


	/** Event when play begins for this actor. */
	virtual void BeginPlay();

	/** Event called every frame */
	virtual void ReceiveTick(float DeltaSeconds);

	/** Detect collisions */
	virtual void ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit);
	
public:
	// How much to rotate the mesh
	static const FRotator rotation;
	void updateMaterial();
};
