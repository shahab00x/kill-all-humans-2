// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include "FloorActor.h"
#include "SideBuilding.generated.h"

/**
 * 
 */
UCLASS()
class MYFPSPROJECT3_API ASideBuilding : public AFloorActor
{
	GENERATED_UCLASS_BODY()



	
public:
	/** after all game elements are created */
	virtual void BeginPlay() override;

	static FRotator getRotation(int num);
	const TCHAR* meshFileReference = TEXT("StaticMesh'/Game/Data/Wall_Window_400x300.Wall_Window_400x300'");
	const TCHAR* textureFileReference = TEXT("Texture2D'/Game/Data/T_Brick_Hewn_Stone_D.T_Brick_Hewn_Stone_D'");
	static const FRotator rotation;
	static const std::vector<FRotator> rotations;
	static const std::vector<FVector> translations;
};
