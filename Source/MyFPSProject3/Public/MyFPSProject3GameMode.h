// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "MyFPSProject3GameMode.generated.h"

UCLASS(minimalapi)
class AMyFPSProject3GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AMyFPSProject3GameMode(const FObjectInitializer& ObjectInitializer);
};



