// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Human.h"
#include <FloorActor.h>
#include <vector>
#include <Engine.h>
#include "GameFramework/Actor.h"
#include "MyPlayer.generated.h"
/**
 * 
 */
UCLASS()
class MYFPSPROJECT3_API AMyPlayer : public AActor
{
	GENERATED_UCLASS_BODY()

	//UPROPERTY()
	//UStaticMeshComponent* floor;

	void logToScreen(FString s, bool clearPrevious = false, float duration = 5.0f){
		if (clearPrevious) GEngine->ClearOnScreenDebugMessages();
		GEngine->AddOnScreenDebugMessage(-1, duration, FColor::Blue, s);
	}
	/** Event called every frame */
	virtual void ReceiveTick(float DeltaSeconds);
	
	/** Event when play begins for this actor. */
	virtual void BeginPlay();

	//UFUNCTION()
	void GenerateMesh(FVector center, FString dir);

	template <class T>
	/** Drop a mesh given by the class T around the player */
	void DropMesh(UClass* staticClass, int meshIndex, FVector center, FString dir, int tileSideLength, FRotator rotation, FVector translation);

	void handleFloorPlacement();
	bool playerIsOnTile(int tileNumber);
	int playerIsOnWhichTile();
	int tilesDistance(std::vector<AActor*>  meshes, int thistile, int thatTile);
	FString isOnMiddleTile();

	/** Gets static mesh component */
	AStaticMeshActor* GetStaticMesh(FString compName);

	UStaticMeshComponent* currentFloor;
	FVector currentFloorsize;
	std::vector<std::vector<AActor*>> meshArray;

	FVector playerPosition;
	int tileSideSize;
	FVector currentCell;
	TSubclassOf<AHuman> humanBlueprint;

};
