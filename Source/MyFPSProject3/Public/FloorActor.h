// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include <Engine.h>
#include "GameFramework/Actor.h"
#include "FloorActor.generated.h"
/**
 * 
 */
UCLASS()
class MYFPSPROJECT3_API AFloorActor : public AActor
{
	GENERATED_UCLASS_BODY()

	
	UPROPERTY()
	UStaticMeshComponent* mainMesh;	

	UPROPERTY()
	UMaterialInstanceDynamic* MaterialInstance;

public:
	/** after all game elements are created */
	virtual void BeginPlay() override;

	void logToScreen(FString s, bool clearPrevious = false, float duration = 5.0f){
		if (clearPrevious) GEngine->ClearOnScreenDebugMessages();
		GEngine->AddOnScreenDebugMessage(-1, duration, FColor::Blue, s);
	}


	const TCHAR* meshFileReference = TEXT("StaticMesh'/Game/Data/Floor_1000x1000.Floor_1000x1000'");
	const TCHAR* textureFileReference = TEXT("Texture2D'/Game/Materials/road_with_sidewalk_4096x4096.road_with_sidewalk_4096x4096'");
	static const FRotator rotation;
	static const std::vector<FRotator> rotations;
	static const std::vector<FVector> translations;
	UFUNCTION()
	void updateMaterial();
	
};
